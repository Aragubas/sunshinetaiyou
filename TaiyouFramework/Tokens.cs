/*
    Copyright 2021 Aragubas

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

namespace TaiyouFramework.Tokens
{
    public abstract class TokenBase
    {
        public string Name;
    }

    public class Token_Namespace : TokenBase { }

    public class InfoBlock
    {
        public string StringRepresentation;
        public object Data;

        public override string ToString() { return StringRepresentation;}
    }

    public enum DataType
    {
        Null, String, Int, Bool, Float, VariableReference, Function
    }

    public enum Denominator
    {
        Null, Super, BlockStart, SpecialBlockEnd, Instruction
    }

}
